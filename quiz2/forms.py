from django import forms
from django.contrib.postgres.forms import JSONField
from .models import Question
from .serializers import AnswerChoiceSerializer


class QuestionForm(forms.ModelForm):
    answer_choices = JSONField(initial=[
        {"id": 1, "text": "a", "correct": True},
        {"id": 2, "text": "b", "correct": False},
        {"id": 3, "text": "c", "correct": False},
        {"id": 4, "text": "d", "correct": False}
    ])

    class Meta:
        model = Question
        fields = ("text", "answer_choices")

    def clean_answer_choices(self):

        if len(self.cleaned_data["answer_choices"]) != 4:
            raise forms.ValidationError("Answer Choices length is not 4")

        answer_choices_serializer = AnswerChoiceSerializer(
            data=self.cleaned_data["answer_choices"], many=True)
        if not answer_choices_serializer.is_valid():
            raise forms.ValidationError(str(answer_choices_serializer.errors))

        answer_choices = []
        for idx, answer_choice in enumerate(answer_choices_serializer.data):
            answer_choices.append(dict(answer_choice, id=idx))

        return answer_choices
