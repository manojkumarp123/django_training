from .forms import QuestionForm
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from .models import Question
from django.urls import reverse


def question_detail(request, pk):
    question = get_object_or_404(Question, pk=pk)
    if request.method == 'POST':
        form = QuestionForm(
            request.POST, request.FILES, instance=question)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(
                reverse('quiz2:index'))
    else:
        form = QuestionForm(instance=question)
    return render(
        request, 'quiz2/question_detail.html',
        {'form': form, "question": question})


def question_delete(request, pk):
    question = get_object_or_404(Question, pk=pk)
    question.delete()
    return HttpResponseRedirect(
            reverse('quiz2:index'))


def question_list(request):
    questions = Question.objects.all()
    if request.method == 'POST':
        form = QuestionForm(
            request.POST, request.FILES)
        if form.is_valid():
            new_form = form.save()
            return HttpResponseRedirect(
                reverse('quiz2:detail', args=(new_form.pk, )))
    else:
        form = QuestionForm()
    return render(
        request, 'quiz2/question_list.html',
        {'questions': questions, "form": form})
