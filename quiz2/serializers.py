from rest_framework import serializers


class AnswerChoiceSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    text = serializers.CharField(max_length=200)
    correct = serializers.BooleanField()
