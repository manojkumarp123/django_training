from django.conf.urls import url
from .views import question_detail, question_list, question_delete

app_name = 'quiz2'
urlpatterns = [
    url(r'^questions2/$', question_list, name="index"),
    url(r'^questions2/(?P<pk>\d+)/$', question_detail, name="detail"),
    url(r'^questions2/(?P<pk>\d+)/delete/$', question_delete, name="delete")
]
