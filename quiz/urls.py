from django.conf.urls import url
from .views import QuestionView, \
    QuestionDetailView, QuestionResultView, \
    answer_choices, QuestionDeleteView

app_name = 'quiz'
urlpatterns = [
    url(
        r'^questions/$',
        QuestionView.as_view(),
        name="index"),
    url(
        r'^questions/(?P<pk>\d+)/$',
        QuestionDetailView.as_view(),
        name="detail"),
    url(
        r'^questions/(?P<pk>\d+)/delete/$',
        QuestionDeleteView.as_view(),
        name="delete"),
    url(
        r'^questions/(?P<pk>\d+)/choice/$',
        answer_choices,
        name="choice"),
    url(
        r'^questions/(?P<pk>\d+)/result/$',
        QuestionResultView.as_view(),
        name="result"),
]
