from django import forms


class AnswerChoiceForm(forms.Form):
    text = forms.CharField(label='Answer Text', max_length=100)
    correct = forms.BooleanField(initial=False, required=False)
