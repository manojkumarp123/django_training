from django.views import generic
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import AnswerChoiceForm
from django.forms import formset_factory
from django.urls import reverse, reverse_lazy
from .models import Question


class QuestionView(generic.ListView):
    """
    List Questions
    Post New Question - Redirect to Question Detail
    """
    template_name = 'index.html'
    context_object_name = 'questions'
    queryset = Question.objects.all()

    def post(self, request, *args, **kwargs):
        question = Question.objects.create(
            text=request.POST["text"],
            answer_choices={})
        return HttpResponseRedirect(
            reverse('quiz:detail', args=(question.id,)))


class QuestionDetailView(generic.DetailView):
    """
    Question Details
    """
    template_name_field = 'question'
    model = Question


class QuestionResultView(generic.DetailView):
    template_name = 'result.html'
    context_object_name = 'question'
    queryset = Question.objects.all()

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        context["result"] = "Right" if self.object.is_answer_choice_id_correct(
            int(request.POST["choice"])) else "Wrong"
        return self.render_to_response(context)


def answer_choices(request, pk):
    AnswerChoiceFormSet = formset_factory(AnswerChoiceForm)
    if request.method == 'POST':
        formset = AnswerChoiceFormSet(request.POST, request.FILES)
        if formset.is_valid():
            answer_choices = []
            for idx, c in zip(
                    range(len(formset.cleaned_data)),
                    formset.cleaned_data):
                answer_choices.append(dict(c, id=idx))
            question = Question.objects.get(id=pk)
            question.answer_choices = answer_choices
            question.save()
            return HttpResponseRedirect(
                reverse('quiz:detail', args=(pk,)))
    else:
        data = {
             'form-TOTAL_FORMS': '4',
             'form-INITIAL_FORMS': '4',
             'form-MAX_NUM_FORMS': '4',
             'form-MAX_NUM_FORMS': '4'
         }
        formset = AnswerChoiceFormSet(data)
    return render(request, 'answer_choices.html', {'formset': formset})


class QuestionDeleteView(generic.DeleteView):
    """
    Delete a Question
    """
    success_url = reverse_lazy('quiz:index')
    template_name_field = "question"
    model = Question
