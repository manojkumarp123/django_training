from django.db import models
from django.contrib.postgres.fields import JSONField


class Question(models.Model):
    text = models.CharField(max_length=200)
    answer_choices = JSONField()

    def correct_answer_choice_ids(self):
        return (c["id"] for c in self.answer_choices if c["correct"])

    def is_answer_choice_id_correct(self, answer_choice_id):
        return answer_choice_id in self.correct_answer_choice_ids()
