from django.conf.urls import url, include
from django.contrib import admin
import quiz.urls
import quiz2.urls

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(quiz.urls)),
    url(r'^', include(quiz2.urls))
]
